#include <cstdio>
#include <cmath>

const int width = 8;
const int height = 6;

const int centerX = 5;
const int centerY = 3;
const int radius = 3;

const int top = 5;
const int right = 3;
const int bottom = 8;
const int left = 6;

double computeDistance(double x1, double y1, double x2, double y2) {
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

bool isOutside(int i, int j, double dx, double dy) {
    double x = j * dx;
    double y = i * dy;
    double distance = computeDistance(x, y, centerX, centerY);

    if (x > top && y > right && distance > radius) {
        return true;
    }
    return false;
}

bool nearCircle(int i, int j, double dx, double dy) {
    return isOutside(i + 1, j, dx, dy) || isOutside(i, j + 1, dx, dy);
}

double computeLambda(int i, int j, double dx, double dy) {
    double x = j * dx;
    double y = i * dy;
    double distance_from_node_to_circle_by_y = sqrt(pow(radius, 2) - pow(x - centerX, 2)) + centerY - y;
    return distance_from_node_to_circle_by_y / dy;
}

double computeMu(int i, int j, double dx, double dy) {
    double x = j * dx;
    double y = i * dy;
    double distance_from_node_to_circle_by_x = sqrt(pow(radius, 2) - pow(y - centerY, 2)) + centerX - x;
    return distance_from_node_to_circle_by_x / dx;
}

void init(int Nx, int Ny, double dx, double dy, double **A, double *B) {
    int total = Nx * Ny;
    for (int i = 0; i < total; ++i) {
        std::fill_n(A[i], total, 0);
    }
    std::fill_n(B, total, 0);
    double temp = 10;
    for (int i = 0; i < Ny; i++) {
        for (int j = 0; j < Nx; j++) {
            int k = i * Nx + j;
            A[k][k] = 1.0;
            if (isOutside(i, j, dx, dy)) {
                B[k] = temp;
            } else if (j == Nx - 1) {       // right
                A[k][k - 1] = -1.0;
                B[k] = 0;
            } else if (j == 0) {            // left
                B[k] = 100;
            } else if (i == 0) {            // bottom
                A[k][k] = 1.0 + 1.0 * dy;
                A[k][k + Nx] = -1.0;
                B[k] = 0;
            } else if (i == Ny - 1) {       // top
                B[k] = 300;
            } else {
                if (nearCircle(i, j, dx, dy)) {
                    double mu = computeMu(i, j, dx, dy);
                    double lambda = computeLambda(i, j, dx, dy);

                    mu = fmin(1.0, fabs(mu));
                    lambda = fmin(1.0, fabs(lambda));

                    A[k][k] = -(1.0 / mu / dx / dx + 1.0 / lambda / dy / dy);
                    A[k][k + Nx] = (1.0 / lambda / (lambda + 1) / dy / dy);
                    A[k][k - Nx] = (1.0 / (lambda + 1) / dy / dy);
                    A[k][k - 1] = (1.0 / mu / (mu + 1) / dx / dx);
                    A[k][k + 1] = (1.0 / (mu + 1) / dx / dx);
                } else {
                    A[k][k] = -2 * (1.0 / dx / dx + 1.0 / dy / dy);
                    A[k][k + Nx] = (1.0 / dy / dy);
                    A[k][k - Nx] = (1.0 / dy / dy);
                    A[k][k - 1] = (1.0 / dx / dx);
                    A[k][k + 1] = (1.0 / dx / dx);
                }
            }
        }
    }
}

void solve(double **A, double *B, double **field, int Nx, int Ny) {
    int total = Nx * Ny;
    for (int i = 0; i < total - 1; ++i) {
        for (int j = i + 1; j < total; ++j) {
            B[j] -= B[i] * A[j][i] / A[i][i];
            for (int k = total - 1; k >= i; --k) {
                A[j][k] -= A[i][k] * A[j][i] / A[i][i];
            }
        }
    }
    for (int i = total - 1; i >= 0; --i) {
        int x = i % Nx;
        int y = i / Nx;

        double sum = B[i];

        for (int j = i + 1; j < total; ++j) {
            int px = j % Nx;
            int py = j / Nx;
            sum -= A[i][j] * field[py][px];
        }
        field[y][x] = sum / A[i][i];
    }
}

void storeTemperatures(double dx, double dy, double **field, int Nx, int Ny) {
    const char *filename = "temperature.txt";
    FILE *f = fopen(filename, "w");

    for (int i = 0; i < Ny; i++) {
        for (int j = 0; j < Nx; j++) {
            auto x = (double) j * (double) width / (Nx - 1);
            auto y = (double) i * (double) height / (Ny - 1);
            if (isOutside(i, j, dx, dy)) {
                fprintf(f, "%lf %lf NaN\n", x, y);
            } else {
                fprintf(f, "%lf %lf %lf\n", x, y, field[i][j]);
            }
        }
        fprintf(f, "\n");
    }
    fclose(f);
}


void storeMatrix(int Nx, int Ny, double **field, double dx, double dy) {
    FILE *matrix_file = fopen("net.txt", "w");
    for (int i = Ny - 1; i >= 0; i--) {
        for (int j = 0; j < Nx; j++) {
            if (isOutside(i, j, dx, dy)) {
                field[i][j] = 0;
            }
            fprintf(matrix_file, "%9.4lf\t", field[i][j]);
        }
        fprintf(matrix_file, "\n");
    }
    fclose(matrix_file);
}

int main() {
    double dx = 0.2;
    double dy = 0.2;

    int Nx = (int) (width / dx) + 1;
    int Ny = (int) (height / dy) + 1;

    int total = Nx * Ny;

    double **A = new double *[total];
    for (int i = 0; i < total; i++) {
        A[i] = new double[total];
    }

    double *B = new double[total];

    double **solution = new double *[Ny];
    for (int i = 0; i < Ny; i++) {
        solution[i] = new double[Nx];
    }

    init(Nx, Ny, dx, dy, A, B);

    solve(A, B, solution, Nx, Ny);
    storeTemperatures(dx, dy, solution, Nx, Ny);
    storeMatrix(Nx, Ny, solution, dx, dy);

    delete[] B;

    for (int i = 0; i < total; i++) {
        delete[] A[i];
    }
    delete[] A;

    for (int i = 0; i < Ny; i++) {
        delete[] solution[i];
    }
    delete[] solution;

    return 0;
}