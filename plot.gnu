#!/usr/bin/gnuplot -persistent
set size square
set output 'plot.eps'
stats 'temperature.txt' using 3
set cbrange [STATS_min:STATS_max]
set cbtics add (STATS_min, STATS_max)
set xrange[0:8]
set yrange[0:6]
set palette rgbformulae 22,13,-31
set pm3d map
set pm3d flush begin ftriangles scansforwar interpolate 4,4
splot 'temperature.txt' using 1:2:3 with pm3d title ''
pause -1
